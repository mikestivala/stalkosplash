@extends('layout')
@section('content')
    <div class="container" style="height: 100%">
<div class="aligner">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1 class="text-uppercase">Stalko</h1>
                <p>Stalko will be launching their second album <strong>A Long Wave Goodbye</strong> on the 9th of April, 2016.</p>
                <p><a target="_blank" href="https://shop.trackagescheme.com/event/stalko-album-launch-palace-theatre-april-9/">Buy tickets</a> | <a href="http://share.stalkoband.com">Hear a song</a></p>
            </div>
        </div>
    </div>
</div>
@endsection